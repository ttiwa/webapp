$(function () {
            var jsondata = JSON.parse($(".swiper-slide-active #json-data").text() || "{}");
            $('.swiper-slide-active .post-meta').froalaEditor({
        key: 'LB2D3E2D2sB4E4A3B12B3D8D6F2D4C3fohtiD-13F4bmn==',
    toolbarInline: true,
    toolbarButtons: ['emoticons', 'undo', 'redo'],
    toolbarButtonsXS: null,
    toolbarButtonsSM: null,
    toolbarButtonsMD: null,
    saveURL: "/api/v1/null"
})
            $('.swiper-slide-active .editor-content').froalaEditor({
        key: 'LB2D3E2D2sB4E4A3B12B3D8D6F2D4C3fohtiD-13F4bmn==',
    toolbarInline: true,
    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'color', 'emoticons', '-', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent', '-', 'insertImage', 'insertLink', 'insertFile', 'insertVideo', 'undo', 'redo'],
    toolbarButtonsXS: null,
    toolbarButtonsSM: null,
    toolbarButtonsMD: null,
    saveURL: '/api/v1/null'
})
            $('.swiper-slide-active .post-meta').on('froalaEditor.save.after', function (e, editor, html) {
        jsondata.title = $(".swiper-slide-active .post-title").text();
    $('.swiper-slide-active .editor-content').froalaEditor('save.save');
});
            $('.swiper-slide-active .editor-content').on('froalaEditor.save.before', function (e, editor, html) {
        jsondata.content = html;
    $.post('/api/v1/posts/save', jsondata, (data) => {
        window.location.reload(true);
    });
});
        });
$("#editorLoading").modal("hide");