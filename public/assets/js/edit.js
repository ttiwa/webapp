jQuery.cachedScript = function( url, options ) {
 
  // Allow user to set any option except for dataType, cache, and url
  options = $.extend( options || {}, {
    dataType: "script",
    cache: true,
    url: url
  });
 
  // Use $.ajax() since it is more flexible than $.getScript
  // Return the jqXHR object so we can chain callbacks
  return jQuery.ajax( options );
};

$("head").append(`<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="./assets/froala/css/froala_editor.css">
<link rel="stylesheet" href="./assets/froala/css/froala_style.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/code_view.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/colors.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/emoticons.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/image_manager.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/image.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/line_breaker.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/quick_insert.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/table.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/file.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/char_counter.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/video.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/emoticons.css">
<link rel="stylesheet" href="./assets/froala/css/plugins/fullscreen.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">`);

$.cachedScript("https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js");
$.cachedScript("https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js");
$.cachedScript("/assets/froala/js/froala_editor.min.js");
$.cachedScript("/assets/froala/js/plugins/align.min.js");
$.cachedScript("/assets/froala/js/plugins/code_beautifier.min.js");
$.cachedScript("/assets/froala/js/plugins/code_view.min.js");
$.cachedScript("/assets/froala/js/plugins/colors.min.js");
$.cachedScript("/assets/froala/js/plugins/emoticons.min.js");
$.cachedScript("/assets/froala/js/plugins/draggable.min.js");
$.cachedScript("/assets/froala/js/plugins/font_size.min.js");
$.cachedScript("/assets/froala/js/plugins/font_family.min.js");
$.cachedScript("/assets/froala/js/plugins/image.min.js");
$.cachedScript("/assets/froala/js/plugins/image_manager.min.js");
$.cachedScript("/assets/froala/js/plugins/line_breaker.min.js");
$.cachedScript("/assets/froala/js/plugins/quick_insert.min.js");
$.cachedScript("/assets/froala/js/plugins/link.min.js");
$.cachedScript("/assets/froala/js/plugins/lists.min.js");
$.cachedScript("/assets/froala/js/plugins/paragraph_format.min.js");
$.cachedScript("/assets/froala/js/plugins/paragraph_style.min.js");
$.cachedScript("/assets/froala/js/plugins/video.min.js");
$.cachedScript("/assets/froala/js/plugins/table.min.js");
$.cachedScript("/assets/froala/js/plugins/url.min.js");
$.cachedScript("/assets/froala/js/plugins/emoticons.min.js");
$.cachedScript("/assets/froala/js/plugins/file.min.js");
$.cachedScript("/assets/froala/js/plugins/entities.min.js");
$.cachedScript("/assets/froala/js/plugins/inline_style.min.js");
$.cachedScript("/assets/froala/js/plugins/save.min.js");
$.cachedScript("/assets/froala/js/plugins/fullscreen.min.js");
$.cachedScript("/assets/js/editor.js");