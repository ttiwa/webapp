/**
 * Main JS file for Zvikov
 */

jQuery.cachedScript = function( url, options ) {
 
  // Allow user to set any option except for dataType, cache, and url
  options = $.extend( options || {}, {
    dataType: "script",
    cache: true,
    url: url
  });
 
  // Use $.ajax() since it is more flexible than $.getScript
  // Return the jqXHR object so we can chain callbacks
  return jQuery.ajax( options );
};

jQuery(document).ready(function($) {

    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
        h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
        activeSlide,
        currentPageNext = 1,
        currentPagePrev = 1,
        pathname = window.location.pathname,
        $result = $('#content .loop .swiper-wrapper'),
        nextPage,
        prevPage,
        countAllPosts,
        // filter = "",
        firstPostId = $('.swiper-slide-active').attr('data-history'),
        firstPostIndex = 0,
        allPosts,
        readLaterPosts = [],
        swiperPosts,
        checkHistoryOnChange,
        noBookmarksMessage = $('.no-bookmarks').html(),
        monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		getPostsPath = "/api/v1/posts",
		data;

	$('#content .loop .swiper-slide').addClass('first');

    setGalleryRation();
	imageInDiv();
    readingTime($('#content .loop .swiper-slide'));

	if (typeof Cookies.get('zvikov-read-later') !== "undefined") {
		readLaterPosts = JSON.parse(Cookies.get('zvikov-read-later'));
	}

	readLaterPosts = readLater($('#content .loop .swiper-slide'), readLaterPosts);

	$(window).on('load', function(event) {

		var editing = false;
		var swipe, keyboard;

		function updateToolbox() {
			if(!editing) {
				if(getCookie("swipe") == "false") {
					$("tr#disable-swipe td w2c").text("啟用");
					$("tr#disable-swipe th w2c").text("啟用");
				} else {
					$("tr#disable-swipe td w2c").text("停用");
					$("tr#disable-swipe th w2c").text("停用");
				}

				if(getCookie("keyboard") == "false") {
					$("tr#disable-keyboard td w2c").text("啟用");
					$("tr#disable-keyboard th w2c").text("啟用");
				} else {
					$("tr#disable-keyboard td w2c").text("停用");
					$("tr#disable-keyboard th w2c").text("停用");
				}
			}

			if(getCookie("touchpad") == "false") {
				$("tr#disable-touchpad td w2c").text("啟用");
				$("tr#disable-touchpad th w2c").text("啟用");
			} else {
				$("tr#disable-touchpad td w2c").text("停用");
				$("tr#disable-touchpad th w2c").text("停用");
			}
		}

		updateToolbox();

		$("tr#disable-swipe").click(() => {
			if(!getCookie("swipe") || getCookie("swipe") == "true") {
				swiperPosts.allowTouchMove = false;
				setCookie("swipe", false);
			} else {
				swiperPosts.allowTouchMove = true;
				setCookie("swipe", true);
			}
			updateToolbox();
		})

		$("tr#disable-keyboard").click(() => {
			if(!getCookie("keyboard") || getCookie("keyboard") == "true") {
				swiperPosts.keyboard.disable();
				setCookie("keyboard", false);
			} else {
				swiperPosts.keyboard.enable();
				setCookie("keyboard", true);
			}
			updateToolbox();
		})

		$("tr#disable-touchpad").click(() => {
			if(!getCookie("touchpad") || getCookie("touchpad") == "true") {
				swiperPosts.mousewheel.disable();
				setCookie("touchpad", false);
			} else {
				swiperPosts.mousewheel.enable();
				setCookie("touchpad", true);
			}
			updateToolbox();
		})

		//for demo
		$(".loginOnly").toggleClass("loginOnly loginShow");

		$("tr#edit-post").click(() => {
			if(!editing) {
				$("#editorLoading").modal("show");
				$.cachedScript("/assets/js/edit.js");
				$("tr#edit-post th w2c").text("儲存");
				$("tr#edit-post td w2c").text("儲存");
				$("#toolbox").modal("hide");
				swipe = getCookie("swipe");
				keyboard = getCookie("keyboard");
				swiperPosts.allowTouchMove = false;
				swiperPosts.keyboard.disable();
				setCookie("swipe", false);
				setCookie("keyboard", false);
				$("tr#disable-keyboard td w2c").text("啟用");
				$("tr#disable-keyboard th w2c").text("啟用（系統將於您編輯文章時強制停用此模式，以確保編輯器的品質）");
				$("tr#disable-keyboard").addClass("action-lock");
				$("tr#disable-swipe td w2c").text("啟用");
				$("tr#disable-swipe th w2c").text("啟用（系統將於您編輯文章時強制停用此模式，以確保編輯器的品質）");
				$("tr#disable-swipe").addClass("action-lock");
				editing = true;
			} else {
				if(swipe != "false") {
					swiperPosts.allowTouchMove = true;
					setCookie("swipe", true);
				}
				if(keyboard != "false") {
					swiperPosts.keyboard.enable();
					setCookie("keyboard", true);
				}
				$('.swiper-slide-active .post-meta').froalaEditor('save.save');
			}
		})

		$("tr#delete-post").click(() => {
			var cpath = window.location.pathname;
			$.post('/api/v1/posts/delete', {slug: cpath.substr(1, cpath.length-6)}, (data) => {
				window.location.replace("/");
			});
		})

		setGalleryRation();

		// if (!$('body').hasClass('home-template')) {

			// Initialize Posts Swiper slider

			swiperPosts = new Swiper('#content .loop .swiper-container', {
				slidesPerView: 1,
				spaceBetween: 30,
				centeredSlides: true,
				autoHeight: true,
				simulateTouch: true,
				allowTouchMove: true,
				navigation: {
			        nextEl: '#content .loop .next a',
			        prevEl: '#content .loop .prev a',
		      	},
				keyboard: {
					enabled: true,
					onlyInViewport: false,
				},
				mousewheel: {
					sensitivity: 4,
					forceToAxis: true,
					invert: true,
				},
			});

			checkHistoryOnChange = 0;

			//go back top when scroll
			swiperPosts.on('slideChangeTransitionStart', function(event) {
				document.getElementsByTagName("header")[0].scrollIntoView();
			});

			// On slide change add new post to history
			swiperPosts.on('slideChangeTransitionEnd', function(event) {
				$('.swiper-wrapper').height($('.swiper-slide-active').height());

				if (checkHistoryOnChange != 1) {
					var value = $('.swiper-slide-active').attr('data-history');
					var url = window.location.origin + '/' + value + '.html';
					history.pushState({ value: value }, null, url);
				};

				checkHistoryOnChange = 0;

				//load new post
				apiRequest((data) => {
					$('.loop .swiper-slide.first').removeClass('first').addClass('loaded');
					activeSlide = swiperPosts.activeIndex;
					if (activeSlide == 1) {
						setTimeout(function () {
							loadPrevPost(data, swiperPosts);
						}, 300);
					};

					if (activeSlide >= (swiperPosts.slides.length - 2)) {
						loadNextPost(data, swiperPosts);
					};
				})
			});

			pathname = pathname.replace(/#(.*)$/g, '').replace('/\//g', '/');

			// If body has class paged load next/prev posts based on the current page number
			if ($('body').hasClass('paged')) {
				currentPageNext = parseInt(pathname.replace(/[^0-9]/gi, ''));
				currentPagePrev = parseInt(pathname.replace(/[^0-9]/gi, ''));
			};

			if (typeof maxPages === 'undefined' || maxPages === null) {
				maxPages = 0;
			};

			// If body has class tag-template filter by current tag
			// if ($('body').hasClass('tag-template')) {
			// 	getPostsPath = getPostsPath + "/tag/" + $('.tag-title').attr('data-tag');
			// };

			// // If body has class author-template filter by current author
			// if ($('body').hasClass('author-template')) {
			// 	getPostsPath = getPostsPath + "/author/" + $('.author').attr('data-author');
			// };

			apiRequest((data) => {
				countAllPosts = data.postCounts;
				maxPages = countAllPosts;

				// Create pagination number
				firstPostIndex = data.index + 1;
				$('.pagination-number').append('<b>' + firstPostIndex + '</b>/' + countAllPosts);

				// if ($('body').hasClass('post-template')) {
				currentPageNext = data.index;
				currentPagePrev = data.index;
				// } else if ($('body').hasClass('subscribe') || $('body').hasClass('page-template') || $('.error-content').length) {
				// 	return;
				// };
				
				// Load new posts
				loadNextPost(data, swiperPosts);
				loadPrevPost(data, swiperPosts);
			})
	});

	// Make arrows sticky
    if (w < 768){
        $(".next, .prev").stick_in_parent({
			offset_top: 100,
		}).on("sticky_kit:bottom", function(e) {
	    	$('.next').addClass('bottom');
	  	}).on("sticky_kit:unbottom", function(e) {
	    	$('.next').removeClass('bottom');
	  	});
    }else{
        $(".next, .prev").stick_in_parent({
			offset_top: 225,
		});
    }

    // Initialize ghostHunter - A Ghost blog search engine
    var searchField = $("#search-field").ghostHunter({
        results             : "#results",
        onKeyUp             : true,
        zeroResultsInfo     : true,
        displaySearchInfo   : false,
        onComplete      : function( results ){

        	$('#results').empty();

        	var tags = [];
        	$.each(results, function(index, val) {
        		if (val.tags.length) {
        			if ($.inArray(val.tags[0].name, tags) === -1) {
        				tags.push(val.tags[0].name);
        			};
        		}else{
        			if ($.inArray('Other', tags) === -1) {
        				tags.push('Other');
        			};
        		};
        	});
        	tags.sort();

        	$.each(tags, function(index, val) {
        		var tag = val;
        		if (val == 'Other') {
        			tag = $('#results').attr('data-other');
        		};
        		$('#results').append('<h5>'+ tag +'</h5><ul data-tag="'+ val +'" class="list-box"</ul>');
        	});

        	$.each(results, function(index, val) {
                var dateSplit = val.pubDate.split(' ')
                var month = monthNames.indexOf(dateSplit[1])+1;
                var date = moment(dateSplit[0]+'-'+month+'-'+dateSplit[2], "DD-MM-YYYY").format('DD MMM YYYY');
        		if (val.tags.length) {
	        		$('#results ul[data-tag="'+ val.tags[0].name +'"]').append('<li><time>'+ date +'</time><a href="#" class="read-later" data-id="'+ val.id +'"></a><a href="'+ val.link +'">'+ val.title +'</a></li>');
        		}else{
        			$('#results ul[data-tag="Other"]').append('<li><a href="#" class="read-later" data-id="'+ val.id +'"></a><time>'+ date +'</time><a href="'+ val.link +'">'+ val.title +'</a></li>');
        		};
        	});

        	readLaterPosts = readLater($('#results'), readLaterPosts);

        }
    });

	$('#search').on('shown.bs.modal', function (e) {
		$('#search-field').focus();
	});

	// Execute on resize
    $(window).on('resize', function(event) {
        w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

        if (w < 768){
        	$(".next, .prev").trigger("sticky_kit:detach");
            $(".next, .prev").stick_in_parent({
				offset_top: 100,
			}).on("sticky_kit:bottom", function(e) {
		    	$('.next').addClass('bottom');
		  	}).on("sticky_kit:unbottom", function(e) {
		    	$('.next').removeClass('bottom');
		  	});
        }else{
        	$(".next, .prev").trigger("sticky_kit:detach");
            $(".next, .prev").stick_in_parent({
				offset_top: 225,
			});
        }

        setTimeout(function() {
			$('.swiper-wrapper').height($('.swiper-slide-active').height());
		}, 300);

    });

    // On back/forward click change slide
	if (window.history && window.history.pushState) {
		$(window).on('popstate', function() {
			checkHistoryOnChange = 1;
			var check = 0;
			$.each(swiperPosts.slides, function(index, val) {
				if (window.history.state != null) {
					if (window.history.state.value == val.dataset.history) {
						swiperPosts.slideTo(index);
					}
					if (window.history.state.value == '') {
						check++;
					};
				}else{
					check++;
				};
			});
			if (check > 0) {
				swiperPosts.slideTo(0);
			};
		});
	}

    // Initialize Highlight.js
    $('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
    });

	// Wrap image in a span element
    function imageInDiv(){
    	$('.swiper-slide:not(.swiper-slide-active) .article-container .post-content img').each(function(index, el) {
    		var parent = $(this).parent();
    		if (!parent.hasClass('img-holder') && !parent.parent().hasClass('img-holder')) {
    			parent.addClass('img-holder');
    			parent.prepend('<span></span>');
    			$(this).prependTo(parent.find('span'));
    		};
    	});
    }

	function prettyDate(date) {
		var d = new Date(date);
		var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		return d.getMonth() + '/' + d.getDate() + ', ' + d.getFullYear();
	};

	function removeValue(arr) {
	    var what, a = arguments, L = a.length, ax;
	    while (L > 1 && arr.length) {
	        what = a[--L];
	        while ((ax= arr.indexOf(what)) !== -1) {
	            arr.splice(ax, 1);
	        }
	    }
	    return arr;
	}

	function readLater(content, readLaterPosts){

		if (typeof readLaterPosts !== "undefined") {
			$.each(readLaterPosts, function(index, val) {
				$('.read-later[data-id="'+ val +'"]').addClass('active');
			});
			bookmarks(readLaterPosts);
		}
		
		$(content).find('.read-later').each(function(index, el) {
			$(this).on('click', function(event) {
				event.preventDefault();
				var id = $(this).attr('data-id');
				if ($(this).hasClass('active')) {
					removeValue(readLaterPosts, id);
				}else{
					readLaterPosts.push(id);
				};
				$('.read-later[data-id="'+ id +'"]').each(function(index, el) {
					$(this).toggleClass('active');
				});
				$('header .btns a .counter').addClass('shake');
				setTimeout(function() {
					$('header .btns a .counter').removeClass('shake');
				}, 300);
				Cookies.set('zvikov-read-later', readLaterPosts, { expires: 365 });
				bookmarks(readLaterPosts);
			});
		});

		return readLaterPosts;

	}

	function bookmarks(readLaterPosts){

		$('.bookmark-container').empty();
		if (readLaterPosts.length) {
			$('.bookmark-container').empty();
			var tags = [];
			if ($.inArray('Other', tags) === -1) {
				tags.push('Other');
			};
			tags.sort();

			$.each(tags, function(index, val) {
				var tag = val;
				if (val == 'Other') {
					tag = $('#results').attr('data-other');
				};
				$('.bookmark-container').append('<h5>'+ tag +'</h5><ul data-tag="'+ val +'" class="list-box"</ul>');
			});

			$.each(readLaterPosts, (index, val) => {
				$.get('/api/v1/posts/all/none/'+val).done(function (data){
					var date = prettyDate(data.update);

					$('.bookmark-container ul[data-tag="Other"]').append('<li><a href="#" class="read-later active" data-id="'+ data.slug +'"></a><time>'+ date +'</time><a href="/'+ data.slug +'.html">'+ data.title +'</a></li>');
				});
			})
			$('.bookmark-container').find('.read-later').each(function(index, el) {
				$(this).on('click', function(event) {
					event.preventDefault();
					var id = $(this).attr('data-id');
					if ($(this).hasClass('active')) {
						removeValue(readLaterPosts, id);
					}else{
						readLaterPosts.push(id);
					};
					$('.read-later[data-id="'+ id +'"]').each(function(index, el) {
						$(this).toggleClass('active');
					});
					Cookies.set('zvikov-read-later', readLaterPosts, { expires: 365 });
					bookmarks(readLaterPosts);
				});
			});
			if (readLaterPosts.length) {
				$('header .counter').removeClass('hidden').text(readLaterPosts.length);
			}else{
				$('header .counter').addClass('hidden');
				$('.bookmark-container').append('<p class="no-bookmarks"></p>');
				$('.no-bookmarks').html(noBookmarksMessage);
			};
		}else{
			$('header .counter').addClass('hidden');
			$('.bookmark-container').append('<p class="no-bookmarks"></p>');
			$('.no-bookmarks').html(noBookmarksMessage);
		};

	}

	function loadNextPost(data, swiperPosts) {
		var nextPage = nextPage;
		if (currentPageNext+1 == data.postCounts) {
			return;
		};

		currentPageNext++;

		// if ($('body').hasClass('paged')) {
		// 	pathname = '/';
		// };
		// nextPage = pathname + 'page/' + currentPageNext + '/';

		// if ($('body').hasClass('post-template')) {
		// 	if (currentPageNext > (data.postCounts - 1)) {
		// 		return;
		// 	};
		nextPage = '/' + data.next.slug + ".html";
		// };

		$.get(nextPage, function (content) {
			var content = $(content);
			//notice following script section
			var currentIndex = parseInt($('.loop .swiper-slide:last-child .pagination-number:first-child b').text()) + 1;
			content.find('.pagination-number').append('<b>' + currentIndex + '</b>/' + countAllPosts);
			readingTime(content.find('#content .loop .swiper-slide'));

			content.find('#content .loop .swiper-slide').addClass('loaded').removeClass('first');
			swiperPosts.appendSlide(content.find('#content .loop .swiper-slide'));
			imageInDiv();

			if (!$('.loop .next a, .loop .prev a').hasClass('active')) {
				$('.loop .next a, .loop .prev a').addClass('active');
			};

			$('pre:not(.hljs)').each(function (i, block) {
				hljs.highlightBlock(block);
			});

			setGalleryRation();

			readLaterPosts = readLater($('#content .loop .swiper-slide:last-child'), readLaterPosts);

		});
	}

	function loadPrevPost(data, swiperPosts) {
		var prevPage = prevPage;
		if (currentPagePrev < 1) {
			return;
		};
		currentPagePrev--;

		// if ($('body').hasClass('paged')) {
		// 	pathname = '/';
		// } else if ($('body').hasClass('home-template') || $('body').hasClass('author-template') || $('body').hasClass('tag-template')) {
		// 	if (currentPagePrev == 0) {
		// 		return;
		// 	};
		// };
		// prevPage = pathname + 'page/' + currentPagePrev + '/';

		// if ($('body').hasClass('post-template')) {
		prevPage = '/' + data.prev.slug + ".html";
		// };
		$.get(prevPage, function (content) {
			var content = $(content);

			var currentIndex = parseInt($('.loop .swiper-slide:first-child .pagination-number:first-child b').text()) - 1;
			content.find('.pagination-number').append('<b>' + currentIndex + '</b>/' + countAllPosts);
			readingTime(content.find('#content .loop .swiper-slide'));

			content.find('#content .loop .swiper-slide').addClass('loaded').removeClass('first');
			swiperPosts.prependSlide(content.find('#content .loop .swiper-slide'));
			imageInDiv();

			if (!$('.loop .next a, .loop .prev a').hasClass('active')) {
				$('.loop .next a, .loop .prev a').addClass('active');
			};

			$('pre:not(.hljs)').each(function (i, block) {
				hljs.highlightBlock(block);
			});

			setGalleryRation();

			readLaterPosts = readLater($('.loop .swiper-slide:first-child'), readLaterPosts);

		});
	}

	function readingTime(content){
		var readingTime = content.find('.reading-time');
		if (readingTime.length) {
			if (readingTime.text() == '< 1 min read') {
				readingTime.text('<1m');
			}else{
				readingTime.text(parseInt(readingTime.text()) + 'm');
			};
			readingTime.removeClass('d-none');
		};
	}
	
	function apiRequest(callback) {
		if (getPostsPath == "/api/v1/posts") {
			getPostsPath = getPostsPath + "/all/none";
		}

		// Fetch posts
		var nowpath = window.location.pathname;
		$.get(getPostsPath + "/" + $('.swiper-slide-active').attr('data-history')).done(function (data){
			callback(data);
		});
	}

	if ($('.fb-comments').length) {
	    var interval = setInterval(function() {
	    	$('.swiper-wrapper').height($('.swiper-slide-active').height());
	    }, 250);
	};

	// Set the right proportion for images inside the gallery
    function setGalleryRation(){
        $('.kg-gallery-image img').each(function(index, el) {
            var container = $(this).closest('.kg-gallery-image');
            var width = $(this)[0].naturalWidth;
            var height = $(this)[0].naturalHeight;
            var ratio = width / height;
            container.attr('style', 'flex: ' + ratio + ' 1 0%');
        });
    }

});