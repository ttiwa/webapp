(function () {
    var fs = require('fs');
    function reset() {
        if(!fs.existsSync('./html/')) {
            fs.mkdirSync('./html/');
            if(fs.existsSync('./html/header.html') && fs.existsSync('./html/footer.html')) {
                fs.unlinkSync('./html/header.html');
                fs.unlinkSync('./html/footer.html');
            }
        }
    }
    function data(name, data) {
        if (data) {
            fs.writeFileSync('./data/' + name + '.json', data);
        } else {
            return JSON.parse(fs.readFileSync('./data/' + name + '.json'));
        }
    }
    function config() {
        return JSON.parse(fs.readFileSync('./data/config.json'));
    }
    function posts() {
        var files = fs.readdirSync(config().publicFolder);
        return files.filter(function (s) { return ~s.indexOf(".html") })
    }
    function post(slug, data) {
        var file = config().publicFolder + slug + ".html"
        if (data) {
            fs.writeFileSync(file, data);
        } else {
            return fs.readFileSync(file);
        }
    }
    function render(name, data) {
        var Handlebars = require('handlebars');
        var template = Handlebars.compile(fs.readFileSync("./views/" + name + ".hbs").toString());
        return template(data);
    }
    function renderPost(data) {
        var tl = require('./lib.js');
        data.data = JSON.stringify(data);
        var post = tl.render('post', data);
        tl.post("content/" + data.slug, post)
        var result = tl.render('default', {
            lang: data.lang,
            title: data.title,
            header: fs.readFileSync('./html/header.html').toString(),
            body: post,
            footer: fs.readFileSync('./html/footer.html').toString()
        });
        tl.post(data.slug, result);
        return tl.post(data.slug);
    }
    function deletePost(slug) {
        fs.unlinkSync(config().publicFolder+slug+'.html');
        fs.unlinkSync(config().publicFolder+'content/'+slug+'.html');
    }
    function init() {
        var tl = require('./lib.js');
        tl.reset();
        fs.writeFileSync("./html/header.html", tl.render("partials/theme_header", { blog: tl.data("blog"), navigation: tl.data("nav") }));
        tl.updatetag();
    }
    function updatetag() {
        var tl = require('./lib.js');
        fs.writeFileSync("./html/footer.html", tl.render("partials/theme_footer", { blog: tl.data("blog"), tags: tl.data("tags") }));
    }
    function auth(email, pwd) {
        var db = require('./db.js');
        return db.auth(email, pwd);
    }
    function random(strlen = 64) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < strlen; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    function gensecret() {
        var config = JSON.parse(fs.readFileSync('./data/config.json'));
        config.sessions.secret = random(64);
        fs.writeFileSync('./data/config.json', JSON.stringify(config));
    }
    function filter(acceptedValues, myObject) {
        var filteredObject = Object.keys(myObject).reduce(function (r, e) {
            if (acceptedValues.includes(myObject[e])) r[e] = myObject[e]
            return r;
        }, {})
        return filteredObject;
    }
    function verifyEmail(email, key) {
        var nodemailer = require('nodemailer');
        var smtpConfig = {
            host: 'smtp.example.com',
            port: 587,
            secure: false, // upgrade later with STARTTLS
            auth: {
                user: 'username',
                pass: 'password'
            }
        };
        var transporter = nodemailer.createTransport(smtpConfig);
        transporter.sendMail({
            from: 'mail@ttiwa.com',
            to: email,
            subject: "Verify your TTiwA account",
            text: "https://ttiwa.com/api/v1/user/verify/" + key,
            html: "https://ttiwa.com/api/v1/user/verify/" + key
        })
    }
    function findDataWithKey(obj, key) {
        return Object.keys(obj).reduce(function (r, k) {
            if (obj[k] && typeof obj[k] === 'object') {
                return r.concat(findDataWithKey(obj[k], key));
            }
            if (k === key) {
                r.push(obj[key]);
            }
            return r;
        }, []);
    }
    //export function
    module.exports.reset = reset;
    module.exports.data = data;
    module.exports.config = config;
    module.exports.posts = posts;
    module.exports.post = post;
    module.exports.render = render;
    module.exports.renderPost = renderPost;
    module.exports.init = init;
    module.exports.updatetag = updatetag;
    module.exports.auth = auth;
    module.exports.random = random;
    module.exports.gensecret = gensecret;
    module.exports.filter = filter;
    module.exports.findDataWithKey = findDataWithKey;
}())