var compressor = require('node-minify');

compressor.minify({
  compressor: 'html-minifier',
  publicFolder: "../",
  input: '*.html',
  output: '$1.html',
  callback: function(err, min) {}
});

compressor.minify({
  compressor: 'html-minifier',
  publicFolder: "../content/",
  input: '*.html',
  output: '$1.html',
  callback: function(err, min) {}
});

compressor.minify({
  compressor: 'gcc',
  publicFolder: "../assets/js/",
  input: '*.js',
  output: '$1.js',
  callback: function(err, min) {}
});

compressor.minify({
  compressor: 'clean-css',
  publicFolder: "../assets/css/",
  input: '*.css',
  output: '$1.css',
  callback: function(err, min) {}
});