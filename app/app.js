'use strict'

//setup express
var express = require('express');
var app = express();

//include custom library
var tl = require('./lib.js');
var tdb = require('./db.js');

//setup session/cookie
tl.gensecret();
//comment above code to use custom secret key in ./data/config.json, else for  security please use our secret generating function
var session = require('express-session');
// app.set('trust proxy', 1);
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: tl.config().sessions.secret,
    cookie: {
        path: "/",
        secure: false,
        maxAge: null
    }
}))

//local login
//setup bodyParser
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var morgan = require('morgan');
app.use(morgan('dev'));

// setup passport
// var passport = require('passport'),
//     LocalStrategy = require('passport-local').Strategy;

// //setup sesssion
// app.use(passport.initialize());
// app.use(passport.session());

//code below

//secure request origin
// app.use('/', (req, res, next) => {
//     if (!tl.config().apis.public) {
//         res.set('Access-Control-Allow-Origin', tl.config.origins.toString());
//     } else {
//         res.set('Access-Control-Allow-Origin', "*");
//     }
//     next();
// })

// app.get('/session', (req, res) => {
//     res.set('Access-Control-Allow-Origin', tl.config().origins.toString());
//     res.json(req.session);
// })

// app.use('/user', (req, res, next) => {
//     res.set('Access-Control-Allow-Origin', tl.config().origins.toString());
//     next();
// })

// //setup passport
// passport.serializeUser(function (user, done) {
//     done(null, user);
// });
// passport.deserializeUser(function (user, done) {
//     done(null, user);
// });
// passport.use(new LocalStrategy({
//     usernameField: "usr",
//     passwordField: "pwd"
// },
//     function (username, password, done) {
//         if (tdb.auth(username, password)) {
//             done(null, username);
//         } else {
//             done(null, false);
//         }
//     }
// ));

// app.post('/user/login',
//     passport.authenticate('local', {
//         successRedirect: '/',
//         failureRedirect: '/login-failed'
//     })
// );

// app.get('/user', (req, res) => {
//     res.send(req.user);
// })

// app.post('/user/signup', (req, res) => {
//     tdb.signup(req.body.name, req.body.pwd, req.body.email);
//     tl.verifyEmail(req.body.email);
//     res.end();
// })

// app.get('user/verify/:key', (req, res) => {
//     tdb.verify(req.params.key);
//     res.end();
// })

//remove on prod
// app.get('/login', (req, res) => {
//     res.sendFile(__dirname + '/public/login.html');
// });
// app.use('/assets', express.static('../assets/'));
//end

tl.init();

app.post('/posts/save', (req, res) => {
    var data = req.body;
    tl.renderPost(data);
    tdb.update(data, (jsonres) => {
        res.redirect(302, '/'+data.slug+'.html');
    })
})

app.post('/posts/delete', (req, res) => {
    tl.deletePost(req.body.slug);
    tdb.delete(req.body.slug);
    res.redirect(302, '/');
})

app.get('/posts/:ftype/:fval/:fkey', (req, res) => {
    var ftype = req.params.ftype,
        fval = req.params.fval,
        fkey = req.params.fkey;
    tdb.search(ftype, fval, fkey, (data) => {
        if(data.failed) {
            res.writeHead(404);
        }
        res.json(data);
    })
})

app.use('/null', (req, res) => {
    res.writeHead(200);
    res.end();
})

app.get('/edit', (req, res) => {
    res.sendFile('./html/edit.html');
})

app.listen(8627);
