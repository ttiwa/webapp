(function () {
    var mongoose = require('mongoose');
    var tl = require('./lib.js');
    // var userdb = mongoose.createConnection('mongodb://localhost:27017/ttiwa-users', { autoIndex: true });
    var mongodb = mongoose.createConnection('mongodb://localhost:27017/ttiwa-posts', {useNewUrlParser: true});
    var options = {
        upsert: true,
        strict: true
    }
    var Schema = mongoose.Schema;
    var postSchema = new Schema({
        "domain": {
            type: String,
            required: true,
            default: tl.config().host
        },
        "slug": {
            type: String,
            required: true
        },
        "date": {
            type: Date,
            default: Date.now,
            required: true
        },
        "cover": String,
        "title": {
            type: String,
            required: true,
            trim: true
        },
        "reading_time": String,
        "content": {
            type: String,
            required: true
        },
        "lang": {
            type: String,
            default: "zh-TW"
        }
    });
    var post = mongodb.model('post', postSchema, 'posts');
    // var userSchema = new Schema({
    //     name: String,
    //     email: String,
    //     pwd: String,
    //     verified: Boolean,
    //     verifyKey: String
    // });
    // var user = userdb.model('user', userSchema);
    module.exports = {
        update: function(data, callback) {
            post.findOne({slug: data.slug}, (err1, res1) => {
                if(res1) {
                    post.updateOne({slug:data.slug}, data, (err3, res3) => {callback(res3)})
                } else {
                    post.create(data, (err2, res2) => {callback(res2)})
                }
            })
        },
        search: function(ftype, fval, fkey, callback) {
            var query = {};
            if(ftype != "all") {
                query[ftype] = fval;
            }
            post.find(query).sort('date').lean().exec((err, res) => {
                var keys = require('./lib.js').findDataWithKey(res, "slug");
                var index = keys.indexOf(fkey);
                if(index == -1) {
                    callback({failed: "post not found"});
                } else {
                    var rdata = res[index];
                    rdata.index = index;
                    rdata.next = res[index + 1];
                    rdata.prev = res[index - 1];
                    post.countDocuments(query, (err, count) => {
                        rdata.postCounts = count;
                        callback(rdata);
                    })
                }
            })
        },
        delete: function(slug) {
            post.deleteOne({slug: slug}, (err) => {})
        }
    }
    // function signup(name, pwd, email) {
    //     var crypto = require('crypto');
    //     var tl = require('./lib.js');
    //     function genkey() {
    //         var key = tl.random();
    //         user.findOne({verifyKey: key}, (err, result) => {
    //             if(!result) {
    //                 return key;
    //             } else {
    //                 return genkey();
    //             }
    //         })
    //     }
    //     var vkey = genkey();
    //     user.create({
    //         name: name,
    //         email: email,
    //         pwd: crypto.createHash('sha256').update(pwd).digest('base64'),
    //         verified: false,
    //         verifyKey: vkey
    //     }, (err) => { });
    //     tl.verifyEmail(email, vkey);
    // }
    // function verify(key) {
    //     user.findOneAndUpdate({verifyKey: key}, {verified:true, verifyKey: null});
    // }
    //export functions
    // module.exports.update = update;
    // module.exports.search = search;
    // module.exports.searchAll = searchAll;
    // module.exports.signup = signup;
    // module.exports.verify = verify;
}())